import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpdateCustModalPage } from './update-cust';

@NgModule({
  declarations: [
    UpdateCustModalPage
  ],
  imports: [
    IonicPageModule.forChild(UpdateCustModalPage)
  ]
})
export class UpdateCustModalPageModule {}